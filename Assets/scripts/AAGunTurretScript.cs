﻿using UnityEngine;
using System.Collections;
using System;

public class AAGunTurretScript : TurretScriptBase {
	protected override float rotationSpeed(){
		return (float)(10*Math.Abs(fromAngle-toAngle)/180 +2);
	}
	
	protected override int constraintAngle(){
		return 100;
	}
}
