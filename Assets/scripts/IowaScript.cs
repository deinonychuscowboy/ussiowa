﻿using UnityEngine;
using System.Collections;
using System;
using Random=UnityEngine.Random;

public class IowaScript : MonoBehaviour {

	public AudioSource audio1;
	public AudioSource audio2;
	public AudioSource audio3;

	public TargetableScript target = null;
	public float attackCD = 0;
	public float turnAngle=0;
	public float vertical=0;

	#region constants
	public const float MAINGUNCD=4;
	public const float AACD=0.5f;
	public const float MACHINECD=0.1f;
	public const float MORTARCD=6;

	const int MAINGUN_DMG = 850;
	const int AA_DMG = 50;
	const int MACHINE_DMG = 2;
	const int MORTAR_DMG = 3200;

	const int MAINGUN_VAR = 1000;
	const int AA_VAR = 100;
	const int MACHINE_VAR = 300;
	const int MORTAR_VAR = 2000;

	const float CRIT_MG_BASE = .05f;
	const float CRIT_MG_SHIP = .1f;
	const float CRIT_MG_AIR = 1f;
	const float CRIT_AA_BASE = 0f;
	const float CRIT_AA_SHIP = 0f;
	const float CRIT_AA_AIR = .2f;
	const float CRIT_MC_BASE = .1f;
	const float CRIT_MC_SHIP = .1f;
	const float CRIT_MC_AIR = .1f;
	const float CRIT_MT_BASE = .15f;
	const float CRIT_MT_SHIP = .10f;
	const float CRIT_MT_AIR = 0f;

	const float MISS_MG_BASE = .1f;
	const float MISS_MG_SHIP = .1f;
	const float MISS_MG_AIR = .95f;
	const float MISS_AA_BASE = .8f;
	const float MISS_AA_SHIP = .8f;
	const float MISS_AA_AIR = .25f;
	const float MISS_MC_BASE = .15f;
	const float MISS_MC_SHIP = .15f;
	const float MISS_MC_AIR = .45f;
	const float MISS_MT_BASE = .25f;
	const float MISS_MT_SHIP = .45f;
	const float MISS_MT_AIR = 1f;

	#endregion

	public enum ATTACK {None, MainGun, AAGun, MachineGun, Mortar};
	public ATTACK currentAttack = ATTACK.None;

	public TargetableScript iowaTarget{
		get{
			Component c = gameObject.GetComponent (typeof(TargetableScript));
			return (TargetableScript)c;
		}
	}


	bool TurretRotation(Type type,float angle){
		Component[] c = gameObject.GetComponentsInChildren (type);
		bool aggregate = true;
		foreach (TurretScriptBase t in c) {
			aggregate=t.Rotate(angle)&&aggregate;
		}
		return aggregate;
	}
	bool TurretInclination(Type type,float angle){
		Component[] c = gameObject.GetComponentsInChildren (type);
		bool aggregate = true;
		foreach (TurretScriptBase t in c) {
			aggregate=t.Incline(angle)&&aggregate;
		}
		return aggregate;
	}
	bool TurretReset(Type type){
		Component[] c = gameObject.GetComponentsInChildren (type);
		bool aggregate = true;
		foreach (TurretScriptBase t in c) {
			aggregate=t.Rotate(t.Forward)&&aggregate;
			aggregate=t.Incline(10)&&aggregate;
			t.ResetLights();
		}
		return aggregate;
	}
	bool Fire(){
		if(!audio3.isPlaying)
			audio3.Play();
		Component[] c = gameObject.GetComponentsInChildren (typeof(TurretScript));
		bool aggregate=false;
		foreach (TurretScript t in c) {
			aggregate=t.Fire()||aggregate;
		}
		return aggregate;
	}
	bool FireMachineGuns(){
		if(!audio1.isPlaying)
			audio1.Play();
		Component[] c = gameObject.GetComponentsInChildren (typeof(MachineGunTurretScript));
		bool aggregate=false;
		foreach (MachineGunTurretScript t in c) {
			aggregate=t.Fire()||aggregate;
		}
		return aggregate;
	}
	bool FireAAGuns(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(AAGunTurretScript));
		bool aggregate=false;
		foreach (AAGunTurretScript t in c) {
			aggregate=t.Fire()||aggregate;
		}
		return aggregate;
	}
	int calculateDamage(ATTACK attack){
		System.Random random = new System.Random();

		TargetableScript.TARGET_TYPE type = target.getTargetType();
		int damage = 0;
		if(attack == ATTACK.MainGun){
			attackCD = MAINGUNCD;
			damage = MAINGUN_DMG + random.Next(0, MAINGUN_VAR);
			 if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_MG_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_MG_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_MG_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_MG_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_MG_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_MG_SHIP)
					damage = 0;
			}
			if(!Fire()){
				damage=0;
			}
		} else if(attack == ATTACK.AAGun){ 
			attackCD = AACD;
			damage = AA_DMG + random.Next(0, AA_VAR);
			if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_AA_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_AA_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_AA_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_AA_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_AA_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_AA_SHIP)
					damage = 0;
			}
			if(!FireAAGuns()){
				damage=0;
			}else if(!audio3.isPlaying)
				audio3.Play();
		} else if(attack == ATTACK.MachineGun){ 
			attackCD = MACHINECD;
			damage = MACHINE_DMG + (int)(random.Next(0, MACHINE_VAR)*25/Vector3.Distance(this.transform.position, this.target.transform.position));
			if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_MC_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_MC_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_MC_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_SHIP)
					damage = 0;
			}
			if(!FireMachineGuns()){
				damage=0;
			}
		} else if(attack == ATTACK.Mortar){
			attackCD = MORTARCD;
			damage = MORTAR_DMG + random.Next(0, MORTAR_VAR);
			if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_MC_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_MC_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_MC_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_SHIP)
					damage = 0;
			}
			if(!audio2.isPlaying)
				audio2.Play();
			//TODO FIRE ANIMATION
		}
		print(damage);
		return damage;
	}

	// Use this for initialization
	void Start () {
		vertical = transform.position.y;
		iowaTarget.textureDisable = true;
	}


	// Update is called once per frame
	void Update () {
		rigidbody.AddRelativeForce (new Vector3 (0, -3000,0));
		rigidbody.AddRelativeTorque (new Vector3 (0, 0, 3000 * turnAngle));

		// Lack of gravity causes weird behavior when you collide with things, so constrain pitch, roll, and vertical position
		transform.Rotate (0, 0, transform.rotation.eulerAngles.z);
		transform.position = new Vector3 (transform.position.x, vertical, transform.position.z);



		if(attackCD > 0){
			attackCD -= Time.deltaTime;
		} else {
			if(target != null && currentAttack != ATTACK.None){
				target.takeDamage(calculateDamage(currentAttack));
			}
		}
		if (target != null) {
			Quaternion rotationSave=transform.rotation;
			transform.LookAt(target.transform.position);
			Vector3 toward=transform.rotation.eulerAngles+(new Vector3(360,360,360)-rotationSave.eulerAngles);
			transform.rotation=rotationSave;
			float toangle=toward.y;
			if (toangle > 180 || toangle < -180) {
				toangle = -1*(360-toangle);
			}
			float distance=Vector3.Distance(transform.position, target.transform.position);
			float inclination=25*distance/100; // TODO define max range for each gun type
			Type t=null;
			if(currentAttack == ATTACK.MainGun){
				t=typeof(TurretScript);
			}else{
				TurretReset(typeof(TurretScript));
			}
			if(currentAttack == ATTACK.MachineGun){
				t=typeof(MachineGunTurretScript);
				inclination=180-(toward.x%360)-90;
			}else{
				TurretReset(typeof(MachineGunTurretScript));
			}
			if(currentAttack == ATTACK.AAGun){
				t=typeof(AAGunTurretScript);
				inclination=180-(toward.x%360)-90;
			}else{
				TurretReset(typeof(AAGunTurretScript));
			}
			if(t!=null){
				TurretRotation (t,toangle);
				TurretInclination (t,inclination);
			}
		}else{
			TurretReset(typeof(TurretScript));
			TurretReset(typeof(MachineGunTurretScript));
			TurretReset(typeof(AAGunTurretScript));
		}
	}
}
