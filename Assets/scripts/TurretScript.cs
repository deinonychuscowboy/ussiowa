﻿using UnityEngine;
using System.Collections;
using System;

public class TurretScript : TurretScriptBase{
	protected override float rotationSpeed(){
		return 1;
	}
	
	protected override int constraintAngle(){
		return 135;
	}
}

public abstract class TurretScriptBase : MonoBehaviour {
	float forward;
	bool rotated;
	bool inclined;
	protected float fromAngle;
	protected float toAngle;
	protected abstract float rotationSpeed();
	protected abstract int constraintAngle();

	public float Forward{get{return this.forward;}}

	// Use this for initialization
	void Start () {
		forward = this.transform.localRotation.eulerAngles.z;
		if (forward < 0)forward += 360;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public bool Rotate(float angle){ // returns true when turret is in position
		fromAngle = transform.localRotation.eulerAngles.z;
		toAngle = angle;
		//Debug.Log(angle+" "+forward);
		int fromSign=1;
		int toSign=1;
		if(forward==180||forward==0){
			fromSign=fromAngle<0?-1:1;
			toSign=toAngle<0?-1:1;
		}else{
			fromSign=forward<0?-1:1;
			toSign=forward<0?-1:1;
		}
		fromAngle-=forward*fromSign;
		toAngle-=forward*toSign;
		toAngle%=360;
		fromAngle%=360;
		//Debug.Log("I: "+fromAngle+", "+toAngle);
		if (fromAngle > 180 || fromAngle < -180) {
			int sign=fromAngle<-180?-1:1;
			fromAngle = (fromAngle-360)*sign;
		}
		if (toAngle > 180 || toAngle < -180) {
			int sign=toAngle<-180?-1:1;
			toAngle = (toAngle-360)*sign;
		}
		toAngle%=360;
		fromAngle%=360;
		//Debug.Log("N: "+fromAngle+", "+toAngle);
		if(fromAngle-toAngle>(1*rotationSpeed()+3)||fromAngle-toAngle<-(1*rotationSpeed()+3)){
			if (constraintAngle()>fromAngle && fromAngle>-1*constraintAngle() ||
				fromAngle > constraintAngle() && fromAngle > toAngle ||
			    fromAngle < -1*constraintAngle() && fromAngle < toAngle) {
				int direction = 1;
				if (fromAngle > toAngle) {
					direction = -1;
				}
				transform.RotateAround(transform.position,Vector3.up,15*rotationSpeed()*direction*Time.deltaTime);
			}
			rotated=false;
			return false;
		}
		rotated=true;
		return true;
	}
	public bool Incline(float angle){
		Component[] c = gameObject.GetComponentsInChildren (typeof(BarrelScript));
		bool aggregate = true;
		foreach (BarrelScript b in c) {
			aggregate=b.Incline(angle)&&aggregate;
		}
		
		c = gameObject.GetComponentsInChildren (typeof(MachineGunBarrelScript));
		foreach (MachineGunBarrelScript b in c) {
			aggregate=b.Incline(angle)&&aggregate;
		}

		c = gameObject.GetComponentsInChildren (typeof(AAGunBarrelScript));
		foreach (AAGunBarrelScript b in c) {
			aggregate=b.Incline(angle)&&aggregate;
		}
		inclined=aggregate;
		return aggregate;
	}
	public bool Fire(){
		if(rotated&&inclined){
			Component[] c = gameObject.GetComponentsInChildren (typeof(BarrelScript));
			foreach (BarrelScript b in c) {
				b.Fire();
			}
			
			c = gameObject.GetComponentsInChildren (typeof(MachineGunBarrelScript));
			foreach (MachineGunBarrelScript b in c) {
				b.Fire();
			}

			c = gameObject.GetComponentsInChildren (typeof(AAGunBarrelScript));
			foreach (AAGunBarrelScript b in c) {
				b.Fire();
			}
		}
		return rotated&&inclined;
	}
	public void ResetLights(){
		// Only necessary for machine guns
		Component[] c = gameObject.GetComponentsInChildren (typeof(MachineGunBarrelScript));
		foreach (MachineGunBarrelScript b in c) {
			b.ResetLights();
		}
	}

}
