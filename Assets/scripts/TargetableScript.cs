﻿using UnityEngine;
using System.Collections;

public class TargetableScript : MonoBehaviour {
	int health=1000;
	public int maxHealth=1000;
	Texture2D arrow=null;
	public bool textureDisable;

	public enum TARGET_TYPE {SHIP, BASE, AIRCRAFT};
	private TARGET_TYPE type;

	// Use this for initialization
	void Start () {
		health=maxHealth;
		arrow=Resources.Load("arrow_out") as Texture2D;
		arrow.anisoLevel=0;
		arrow.filterMode=FilterMode.Trilinear;
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			Component[] c = gameObject.GetComponentsInChildren (typeof(FlameScript));
			foreach(FlameScript e in c){
				e.Fire();
			}
			Object.Destroy(this.gameObject,1);
		}
	}

	void OnGUI(){
		if (!textureDisable)
		{
			GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint(transform.position).x - 16
		                         , Screen.height - (Camera.main.WorldToScreenPoint(transform.position).y) - 16 - 50
		                         , 32, 32), arrow);
		}
	}

	public void takeDamage(int x){
		print(x);
		health = health - x;
		if(health<0){
			health=0;
		}
	}

	public int getHealth(){
		return health;
	}
	public float getHealthRatio(){
		return health/(float)maxHealth;
	}

	public TARGET_TYPE getTargetType(){
		return TARGET_TYPE.BASE;
	}

}
