﻿using UnityEngine;
using System.Collections;
using System;

public class HitAreaScript : MonoBehaviour {

	public GameObject splosion=null;
	SplosionClass sc=null;

	// Use this for initialization
	void Start () {
		sc=new SplosionClass(splosion,new Vector3(0,0,0));
	}
	
	// Update is called once per frame
	void Update () {
		sc.Update();// TODO timeout
	}
}

public class SplosionClass{
	GameObject one;
	GameObject two;
	GameObject three;
	float y;
	public SplosionClass(GameObject splosion,Vector3 position){
		one=GameObject.Instantiate(splosion) as GameObject;
		two=GameObject.Instantiate(splosion) as GameObject;
		three=GameObject.Instantiate(splosion) as GameObject;
		one.transform.position=position;
		two.transform.position=position;
		three.transform.position=position;
		y=position.y+1;
	}
	public void Update(){
		float twophase=(float)Math.Sin(2f/3f*Math.PI+((Time.realtimeSinceStartup/4)%1f)*Math.PI*2);
		float fourphase=(float)Math.Sin(4f/3f*Math.PI+((Time.realtimeSinceStartup/4)%1f)*Math.PI*2);
		float sixphase=(float)Math.Sin(0f/3f*Math.PI+((Time.realtimeSinceStartup/4)%1f)*Math.PI*2);
		Debug.Log(twophase);
		Debug.Log(fourphase);
		Debug.Log(sixphase);
		one.transform.localScale=new Vector3(twophase,twophase,twophase);
		two.transform.localScale=new Vector3(fourphase,fourphase,fourphase);
		three.transform.localScale=new Vector3(sixphase,sixphase,sixphase);
		one.transform.position=new Vector3(one.transform.position.x,y+fourphase,one.transform.position.z);
		two.transform.position=new Vector3(two.transform.position.x,y+sixphase,two.transform.position.z);
		three.transform.position=new Vector3(three.transform.position.x,y+twophase,three.transform.position.z);
	}
}
