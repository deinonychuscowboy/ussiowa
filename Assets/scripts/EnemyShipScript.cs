﻿using UnityEngine;
using System.Collections;

public class EnemyShipScript : MonoBehaviour {

	public TargetableScript target = null;
	public float attackCD = 0;
	public float turnAngle=0;
	public float vertical=0;

	#region constants
	public const float MAINGUNCD=4;
	public const float AACD=1;
	public const float MACHINECD=0.1f;
	public const float MORTARCD=10;

	const int MAINGUN_DMG = 450;
	const int AA_DMG = 50;
	const int MACHINE_DMG = 5;
	const int MORTAR_DMG = 1200;

	const int MAINGUN_VAR = 100;
	const int AA_VAR = 100;
	const int MACHINE_VAR = 50;
	const int MORTAR_VAR = 300;

	const float CRIT_MG_BASE = .05f;
	const float CRIT_MG_SHIP = .1f;
	const float CRIT_MG_AIR = 1f;
	const float CRIT_AA_BASE = 0f;
	const float CRIT_AA_SHIP = 0f;
	const float CRIT_AA_AIR = .2f;
	const float CRIT_MC_BASE = .1f;
	const float CRIT_MC_SHIP = .1f;
	const float CRIT_MC_AIR = .1f;
	const float CRIT_MT_BASE = .15f;
	const float CRIT_MT_SHIP = .10f;
	const float CRIT_MT_AIR = 0f;

	const float MISS_MG_BASE = .1f;
	const float MISS_MG_SHIP = .1f;
	const float MISS_MG_AIR = .95f;
	const float MISS_AA_BASE = .5f;
	const float MISS_AA_SHIP = .5f;
	const float MISS_AA_AIR = .25f;
	const float MISS_MC_BASE = .15f;
	const float MISS_MC_SHIP = .15f;
	const float MISS_MC_AIR = .15f;
	const float MISS_MT_BASE = .15f;
	const float MISS_MT_SHIP = .35f;
	const float MISS_MT_AIR = 1f;

	#endregion

	public enum ATTACK {None, MainGun, AAGun, MachineGun, Mortar};
	public ATTACK currentAttack = ATTACK.None;

	bool TurretRotation(float angle){
		Component[] c = gameObject.GetComponentsInChildren (typeof(TurretScript));
		bool aggregate = true;
		foreach (TurretScript t in c) {
			aggregate=t.Rotate(angle)&&aggregate;
		}
		return aggregate;
	}
	bool TurretInclination(float angle){
		Component[] c = gameObject.GetComponentsInChildren (typeof(TurretScript));
		bool aggregate = true;
		foreach (TurretScript t in c) {
			aggregate=t.Incline(angle)&&aggregate;
		}
		return aggregate;
	}
	bool TurretReset(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(TurretScript));
		bool aggregate = true;
		foreach (TurretScript t in c) {
			aggregate=t.Rotate(t.Forward)&&aggregate;
			aggregate=t.Incline(10)&&aggregate;
		}
		return aggregate;
	}
	void Fire(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(TurretScript));
		foreach (TurretScript t in c) {
			t.Fire();
		}
	}
	int calculateDamage(ATTACK attack){
		System.Random random = new System.Random();

		TargetableScript.TARGET_TYPE type = target.getTargetType();
		int damage = 0;
		if(attack == ATTACK.MainGun){
			attackCD = MAINGUNCD;
			damage = MAINGUN_DMG + random.Next(0, MAINGUN_VAR);
			/* if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_MG_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_MG_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_MG_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_MG_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_MG_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_MG_SHIP)
					damage = 0;
			} */
			Fire();
		} else if(attack == ATTACK.AAGun){ 
			attackCD = AACD;
			damage = AA_DMG + random.Next(0, AA_VAR);
			if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_AA_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_AA_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_AA_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_AA_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_AA_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_AA_SHIP)
					damage = 0;
			}

			//TODO FIRE ANIMATION
		} else if(attack == ATTACK.MachineGun){ 
			attackCD = MACHINECD;
			damage = MACHINE_DMG + random.Next(0, MACHINE_VAR);
			if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_MC_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_MC_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_MC_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_SHIP)
					damage = 0;
			}
			
			//TODO FIRE ANIMATION
		} else if(attack == ATTACK.Mortar){
			attackCD = MORTARCD;
			damage = MORTAR_DMG + random.Next(0, MORTAR_VAR);
			if(type == TargetableScript.TARGET_TYPE.AIRCRAFT){
				if(random.NextDouble() < CRIT_MC_AIR)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_AIR)
					damage = 0;
			} else if(type == TargetableScript.TARGET_TYPE.BASE){
				if(random.NextDouble() < CRIT_MC_BASE)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_BASE)
					damage = 0;
			} else if (type == TargetableScript.TARGET_TYPE.SHIP){
				if(random.NextDouble() < CRIT_MC_SHIP)
					damage *= 2;
				if(random.NextDouble() < MISS_MC_SHIP)
					damage = 0;
			}
			
			//TODO FIRE ANIMATION
		}
		print(damage);
		return damage;
	}

	// Use this for initialization
	void Start () {
		vertical = transform.position.y;
	}


	// Update is called once per frame
	void Update () {
		rigidbody.AddRelativeForce (new Vector3 (0, -3000,0));
		rigidbody.AddRelativeTorque (new Vector3 (0, 0, 3000 * turnAngle));

		// Lack of gravity causes weird behavior when you collide with things, so constrain pitch, roll, and vertical position
		transform.Rotate (0, 0, transform.rotation.eulerAngles.z);
		transform.position = new Vector3 (transform.position.x, vertical, transform.position.z);



		if(attackCD > 0){
			attackCD -= Time.deltaTime;
		} else {
			if(target != null && currentAttack != ATTACK.None){
				if(currentAttack == ATTACK.MainGun){
					Vector3 newPosition = Random.insideUnitCircle * 16;
					Vector3 shipPos = target.transform.position;
					newPosition.x += shipPos.x;
					newPosition.z += shipPos.z;
					newPosition.y = shipPos.y;
					GameObject damage_container = GameObject.Find("Incoming_Damage");
					GameObject gobj = Instantiate (Resources.Load("Prefabs/target_prefab"), newPosition, Quaternion.identity) as GameObject;
					DamageHandler container = damage_container.GetComponent<DamageHandler>();
					gobj.transform.RotateAround(gobj.transform.position, Vector3.right, 90);
					gobj.transform.parent = damage_container.transform;
					TargetScript t = gobj.GetComponent<TargetScript>();
					t.setValues(2, target, calculateDamage(currentAttack));
				}
				else {
					target.takeDamage(calculateDamage(currentAttack));
				}
			}
		}
		if (target != null) {
			Quaternion rotationSave=transform.rotation;
			transform.LookAt(target.transform.position);
			Vector3 toward=transform.rotation.eulerAngles+(new Vector3(360,360,360)-rotationSave.eulerAngles);
			transform.rotation=rotationSave;
			float toangle=toward.y;
			if (toangle > 180 || toangle < -180) {
				toangle = -1*(360-toangle);
			}
			float distance=Vector3.Distance(transform.position, target.transform.position);
			float inclination=45*distance/100; // TODO define max range for each gun type
			if(currentAttack == ATTACK.MainGun){
				TurretRotation (toangle);
				TurretInclination (inclination);
			}else{
				TurretReset();
			}
		}
	}
}
