﻿using UnityEngine;
using System.Collections;

public class TargetScript : MonoBehaviour {
	public float time;
	public TargetableScript target;
	public int damage;
	public bool isSet = false;

	// Use this for initialization
	void Start () {

	}

	public void setValues(float t, TargetableScript s, int d){
		time = t;
		target = s;
		damage = d;
		isSet = true;
	}

	// Update is called once per frame
	void Update () {
		if (isSet)
		{
			time -= Time.deltaTime;
			if (time < 0)
			{
				float pos = Vector3.Distance(this.transform.position, target.transform.position);
				if(pos < 3)
					target.takeDamage(2*damage);
				else if(pos < 9)
					target.takeDamage(damage);

				audio.Play();
				this.Destroy();
			}
		}
	}

	void Destroy(){
		Destroy(gameObject);
	}
}
