﻿using UnityEngine;
using System.Collections;

public class InterfaceScript : MonoBehaviour {
	IowaScript iowa = null;
	GameObject clicked = null;
	Texture2D mainGunButton=null;
	Texture2D mainGunButtonF=null;
	Texture2D machineGunButton=null;
	Texture2D machineGunButtonF=null;
	Texture2D mortarButton=null;
	Texture2D mortarButtonF=null;
	Texture2D aaGunButton=null;
	Texture2D aaGunButtonF=null;
	Texture2D arrow=null;
	Texture2D machinegun=null;
	Texture2D maingun=null;
	Texture2D aagun=null;
	Texture2D mortar=null;
	Texture2D progressbar=null;
	
	IowaScript.ATTACK UIClick;
	
	private float lastCameraDrag=-1;
	
	// Use this for initialization
	void Start () {
		iowa = (IowaScript)this.GetComponent<IowaScript>();
		mainGunButton=Resources.Load("maingunbutton") as Texture2D;
		mainGunButtonF=Resources.Load("maingunbutton_f") as Texture2D;
		machineGunButton=Resources.Load("machinegunbutton") as Texture2D;
		machineGunButtonF=Resources.Load("machinegunbutton_f") as Texture2D;
		mortarButton=Resources.Load("mortarbutton") as Texture2D;
		mortarButtonF=Resources.Load("mortarbutton_f") as Texture2D;
		aaGunButton=Resources.Load("aagunbutton") as Texture2D;
		aaGunButtonF=Resources.Load("aagunbutton_f") as Texture2D;
		arrow=Resources.Load("arrow") as Texture2D;
		aagun=Resources.Load("aagun") as Texture2D;
		machinegun=Resources.Load("machgun") as Texture2D;
		maingun=Resources.Load("maingun") as Texture2D;
		mortar=Resources.Load("mortar") as Texture2D;
		mainGunButton.anisoLevel=0;
		mainGunButton.filterMode=FilterMode.Trilinear;
		mainGunButtonF.anisoLevel=0;
		mainGunButtonF.filterMode=FilterMode.Trilinear;
		machineGunButton.anisoLevel=0;
		machineGunButton.filterMode=FilterMode.Trilinear;
		machineGunButtonF.anisoLevel=0;
		machineGunButtonF.filterMode=FilterMode.Trilinear;
		aaGunButton.anisoLevel=0;
		aaGunButton.filterMode=FilterMode.Trilinear;
		aaGunButtonF.anisoLevel=0;
		aaGunButtonF.filterMode=FilterMode.Trilinear;
		mortarButton.anisoLevel=0;
		mortarButton.filterMode=FilterMode.Trilinear;
		mortarButtonF.anisoLevel=0;
		mortarButtonF.filterMode=FilterMode.Trilinear;
		arrow.anisoLevel=0;
		arrow.filterMode=FilterMode.Trilinear;
		progressbar=Resources.Load("green") as Texture2D;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) 
		{
			RaycastHit hit;
			
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
			{
				Debug.Log(hit.point, this);
				print (hit.collider.gameObject.GetComponent<Component>());
				
				if (hit.collider.gameObject.GetComponent<Component>() != null)
					clicked = hit.collider.gameObject;
				else
					clicked = null;
			}
			
			if(clicked != null && clicked.GetComponent<TargetableScript>() && clicked != this.gameObject){
				iowa.target = clicked.GetComponent<TargetableScript>();
				clicked = null;
			}
		}
	}
	
	void OnGUI () {
		
		int paneWidth=(int)(Screen.width*0.15);
		
		
		if(Input.GetMouseButton(0)
		   &&Input.mousePosition.x<Screen.width-paneWidth*2
		   &&Screen.height-Input.mousePosition.y<Screen.height*0.9f){
			ProcessCameraDrag(Input.mousePosition.x);
		}else{
			ProcessCameraDrag(-1);
		}
		
		Texture2D tex=UIClick==IowaScript.ATTACK.MainGun?mainGunButtonF:mainGunButton;
		GUI.skin.button.normal.background=tex;
		GUI.skin.button.active.background=tex;
		GUI.skin.button.focused.background=tex;
		GUI.skin.button.hover.background=tex;
		if (GUI.Button (new Rect (Screen.width-paneWidth*2, 0, paneWidth, Screen.height/2),"")) {
			iowa.currentAttack = IowaScript.ATTACK.MainGun;
			iowa.attackCD = IowaScript.MAINGUNCD;
			UIClick=IowaScript.ATTACK.MainGun;
		}
		
	tex=UIClick==IowaScript.ATTACK.AAGun?aaGunButtonF:aaGunButton;
		GUI.skin.button.normal.background=tex;
		GUI.skin.button.active.background=tex;
		GUI.skin.button.focused.background=tex;
		GUI.skin.button.hover.background=tex;
		if (GUI.Button (new Rect (Screen.width-paneWidth, 0, paneWidth, Screen.height/2), "")) {
			iowa.currentAttack = IowaScript.ATTACK.AAGun;
			iowa.attackCD = IowaScript.AACD;
			UIClick=IowaScript.ATTACK.AAGun;
		}
		
	tex=UIClick==IowaScript.ATTACK.MachineGun?machineGunButtonF:machineGunButton;
		GUI.skin.button.normal.background=tex;
		GUI.skin.button.active.background=tex;
		GUI.skin.button.focused.background=tex;
		GUI.skin.button.hover.background=tex;
		if (GUI.Button (new Rect (Screen.width-paneWidth*2, Screen.height/2, paneWidth, Screen.height/2), "")) {
			iowa.currentAttack = IowaScript.ATTACK.MachineGun;
			iowa.attackCD = IowaScript.MACHINECD;
			UIClick=IowaScript.ATTACK.MachineGun;
			
		}
		
	tex=UIClick==IowaScript.ATTACK.Mortar?mortarButtonF:mortarButton;
		GUI.skin.button.normal.background=tex;
		GUI.skin.button.active.background=tex;
		GUI.skin.button.focused.background=tex;
		GUI.skin.button.hover.background=tex;
		if (GUI.Button (new Rect (Screen.width-paneWidth, Screen.height/2, paneWidth, Screen.height/2), "")) {
			iowa.currentAttack = IowaScript.ATTACK.Mortar;
			iowa.attackCD = IowaScript.MORTARCD;
			UIClick=IowaScript.ATTACK.Mortar;
		}
		GUI.skin.button.normal.background=null;
		GUI.skin.button.active.background=null;
		GUI.skin.button.focused.background=null;
		GUI.skin.button.hover.background=null;
		
		if(iowa.target!=null){
			Texture2D decal=null;
			switch(iowa.currentAttack){
				case IowaScript.ATTACK.MainGun:
					decal=maingun;
					break;
				case IowaScript.ATTACK.MachineGun:
					decal=machinegun;
					break;
				case IowaScript.ATTACK.Mortar:
					decal=mortar;
					break;
				case IowaScript.ATTACK.AAGun:
					decal=aagun;
					break;
			}
			GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint(iowa.target.transform.position).x-16
			                         ,Screen.height-(Camera.main.WorldToScreenPoint(iowa.target.transform.position).y)-16-50
			                         ,32,32),arrow);
			if(decal!=null){
				GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint(iowa.target.transform.position).x-16+5
				                         ,Screen.height-(Camera.main.WorldToScreenPoint(iowa.target.transform.position).y)-16-90
				                         ,32,32),decal);
			}
			DrawProgressBar(new Vector2(Camera.main.WorldToScreenPoint(iowa.target.transform.position).x
			                            ,Screen.height-(Camera.main.WorldToScreenPoint(iowa.target.transform.position).y)-70),false,iowa.target.getHealthRatio());
		}
		DrawProgressBar(new Vector2(Screen.width*0.35f,20),true,iowa.iowaTarget.getHealthRatio());
		
		iowa.turnAngle=GUI.HorizontalSlider (new Rect (0.1f * Screen.width, Screen.height*0.925f, 0.5f*Screen.width, Screen.height*0.075f), iowa.turnAngle, -1f, 1f);
	}
	
	private void ProcessCameraDrag(float x){
		if(x>=0&&lastCameraDrag>=0&&(x-lastCameraDrag)!=0){
			Camera.main.transform.RotateAround (iowa.transform.position,Vector3.up,(x-lastCameraDrag)*0.5f);
		}
		lastCameraDrag=x;
	}

	private void DrawProgressBar(Vector2 where,bool big,float fill){
		float width=(big?0.6f:0.1f)*Screen.width;
		float height=width/(big?30f:10f);
		GUI.Box(new Rect(where.x-width/2,where.y-height/2,width,height),"");
		float border=4;
		float innerWidth=(width-border*2)*fill;
		float innerHeight=height-border*2;
		GUI.DrawTexture(new Rect(where.x-innerWidth/2,where.y-innerHeight/2,innerWidth,innerHeight),progressbar);
	}
	
}