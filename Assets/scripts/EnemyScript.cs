﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
	EnemyShipScript me;
	IowaScript enemy;

	// Use this for initialization
	void Start () {
		me = (EnemyShipScript)this.GetComponent<EnemyShipScript>();
		GameObject uss_iowa = GameObject.Find("USS Iowa");
		if(uss_iowa != null)
			me.target = uss_iowa.GetComponent<TargetableScript>();
		me.currentAttack = EnemyShipScript.ATTACK.MainGun;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
