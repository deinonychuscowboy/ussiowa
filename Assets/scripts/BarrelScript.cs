﻿using UnityEngine;
using System.Collections;

public class BarrelScript : MonoBehaviour {
	static float defaultIncline=10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public bool Incline(float angle){
		float fromAngle = transform.localRotation.eulerAngles.x -180;
		float toAngle = 90 + angle;
		//Debug.Log("I: "+fromAngle+", "+toAngle);
		if(fromAngle-toAngle>2||fromAngle-toAngle<-2){
			if (150>fromAngle && fromAngle>90 ||
			    fromAngle > 150 && fromAngle > toAngle ||
			    fromAngle < 90 && fromAngle < toAngle) {
				int direction = -1;
				if (fromAngle > toAngle) {
					direction = 1;
				}
				transform.Rotate(new Vector3(5*direction*Time.deltaTime,0,0));
			}
			return false;
		}
		return true;
	}
	public void Fire(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(ExplosionScript));
		foreach(ExplosionScript e in c){
			e.Fire();
		}
	}
}
