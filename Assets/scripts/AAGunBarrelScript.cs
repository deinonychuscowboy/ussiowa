﻿using UnityEngine;
using System.Collections;

public class AAGunBarrelScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public bool Incline(float angle){
		return true;
	}
	public void Fire(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(ExplosionScript));
		foreach(ExplosionScript e in c){
			e.Fire();
		}
	}
	public void ResetLights(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(ExplosionScript));
		foreach(ExplosionScript e in c){
			e.ResetLights();
		}
	}
}
