﻿using UnityEngine;
using System.Collections;
using System;

public class MachineGunTurretScript : TurretScriptBase {
	protected override float rotationSpeed(){
		return (float)(2*Math.Abs(fromAngle-toAngle)/180 +3);
	}
	protected override int constraintAngle(){
		return 160;
	}
}
