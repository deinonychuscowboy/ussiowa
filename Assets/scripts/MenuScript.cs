﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

	GUIText t;

	// Use this for initialization
	void Start () {
		//t.text = "Tutorial";
		//t.fontSize = 30;
		//t.font = new Font("Arial");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI()
	{
		if (GUI.Button(new Rect(Screen.width / 4, (Screen.height / 8) * 6, Screen.width / 5, Screen.height / 5), "Tutorial"))
		{
			Application.LoadLevel("tutorial");
		}

		if (GUI.Button(new Rect((Screen.width / 4) * 2, (Screen.height / 8) * 6, Screen.width / 5, Screen.height / 5), "Level 1"))
		{
			Application.LoadLevel("level1");
		}
	}
}
