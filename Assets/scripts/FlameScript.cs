﻿using UnityEngine;
using System.Collections;

public class FlameScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Fire(){
		this.particleEmitter.Emit();
		this.animation.Play();
		Component[] c = gameObject.GetComponentsInChildren (typeof(LightScript));
		foreach(LightScript e in c){
			e.Fire();
		}
	}
	public void ResetLights(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(LightScript));
		foreach(LightScript e in c){
			e.ResetLights();
		}
	}

}
