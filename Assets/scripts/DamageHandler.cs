﻿using UnityEngine;
using System.Collections;

public class DamageHandler : MonoBehaviour {

	class damage_container {
		public float time;
		public GameObject child;
		public int baseDamage;
		public damage_container(float t, GameObject c, int d){
			time = t;
			child = c;
			baseDamage = d;
		}
	}
	public ArrayList damage_list = new ArrayList();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		for (int i = damage_list.Count - 1; i >= 0; i--)
		{
			damage_container x = (damage_container)damage_list[i];
			x.time -=  Time.deltaTime;
			if(x.time < 0)
				Destroy(x.child);
			damage_list.Remove(x);
		}
	}

	public void addIncomingDamage(GameObject gameO){
		gameO.transform.parent = this.transform;
		damage_list.Add(new damage_container(2, gameO, 500));
	}
}
