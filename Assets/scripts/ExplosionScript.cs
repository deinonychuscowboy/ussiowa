﻿using UnityEngine;
using System.Collections;

public class ExplosionScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Fire(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(FlameScript));
		foreach(FlameScript e in c){
			e.Fire();
		}
	}
	public void ResetLights(){
		Component[] c = gameObject.GetComponentsInChildren (typeof(FlameScript));
		foreach(FlameScript e in c){
			e.ResetLights();
		}
	}

}
